using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestData : MonoBehaviour
{
    [SerializeField] private int _coins;
    [SerializeField] private int _hp;
    void Start()
    {
        _hp = PlayerPrefs.GetInt("HP", 3);
        _coins = PlayerPrefs.GetInt("Coin", 0);
        Debug.Log(_coins);
        PlayerPrefs.SetInt("Coin", 90);
        //PlayerPrefs.DeleteKey("Coin");
    }

}
