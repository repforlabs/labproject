using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsData : MonoBehaviour
{
    private int _hp = 3;
    private int _def = 5;

    private void Awake()
    {
        _hp = PlayerPrefs.GetInt("HP", 3);
        _def = PlayerPrefs.GetInt("Def", 5);
    }

    public void SetHp(int hp)
    {
        _hp = hp;
        SaveData();
    }

    public void TakeHPDamage(int dmg)
    {
        _hp -= dmg;
        SaveData();
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt("Def", _def);
        PlayerPrefs.SetInt("HP", _hp);
        PlayerPrefs.Save();
    }
}
