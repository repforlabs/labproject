using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageManager : MonoBehaviour
{
    [SerializeField] private Image _image;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            _image.color = new Color(1, 0, 0, 1);
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            _image.color = new Color(0, 1, 0, 1);
        }
    }
}
