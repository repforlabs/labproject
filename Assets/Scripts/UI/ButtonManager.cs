using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private Image _image;
    [SerializeField] private GameObject _owlImage;
    void Start()
    {
        _button.onClick.AddListener(ChangeImageColor);
        _button.onClick.AddListener(SwitchOwlImage);
    }

    private void ChangeImageColor()
    {
        _image.color = new Color(0.3f, 0, 1, 1);
    }

    private void SwitchOwlImage()
    {
        _owlImage.SetActive(!_owlImage.activeSelf);
    }

    //������� 2 ������ � �������� �� �������� �������
    //������� bool � ����������� � � ������ SwitchOwlImage
}
