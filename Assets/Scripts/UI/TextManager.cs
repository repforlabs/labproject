using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextManager : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    [SerializeField] private int _coinsCount;
    void Start()
    {
        _text.color = new Color(0.5f, 0, 1, 1);
    }

    void Update()
    {
        _text.text = $"Coins: <b>{_coinsCount}";
    }
}
