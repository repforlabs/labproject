using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpPower;
    [SerializeField] private PlayerAnimationsLauncher _playerAnimationsLauncher;
    private bool _onGround = true;
    void Update()
    {
        PlayJump();
        PlayerMove();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            _onGround = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            _onGround = false;
        }
    }
    private void PlayJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _onGround)
        {
            _rigidbody.AddForce(Vector3.up * _jumpPower, ForceMode.Impulse);
            _playerAnimationsLauncher.PlayJump();
        }
    }

    private void PlayerMove()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        if(z != 0) { 
        _rigidbody.velocity = _speed * transform.forward * z ;//new Vector3(x * _speed, _rigidbody.velocity.y, z * _speed);
        }       
        _playerAnimationsLauncher.PlayWalk(z != 0 || x != 0);
    }

    private void OldInput()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            _rigidbody.velocity = Vector3.forward * _speed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.velocity = Vector3.back * _speed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.velocity = Vector3.right * _speed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.velocity = Vector3.left * _speed;
        }
    }
}
