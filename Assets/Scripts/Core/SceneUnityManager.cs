using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUnityManager : MonoBehaviour
{
    private const int TEST_SCENE = 2;
    private const string WATTER_BALLS_SCENE = "WaterBalls";

    // Start is called before the first frame update
    void Start() 
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(WATTER_BALLS_SCENE);
        }
    }
}
