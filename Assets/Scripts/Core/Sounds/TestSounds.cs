using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TestSounds : MonoBehaviour
{
    [SerializeField] private AudioSource _effectsAudioSource;
    [SerializeField] private AudioMixer _audioMixer;
    void Start()
    {
        _effectsAudioSource.mute = true;
        _audioMixer.SetFloat("MASTER", 20);
        _audioMixer.SetFloat("MUSIC", -30);
        _audioMixer.SetFloat("EFFECTS", -40);
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.K))
        {
            _effectsAudioSource.Play();
        }
    }
}
