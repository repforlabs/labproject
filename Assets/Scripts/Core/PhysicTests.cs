using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicTests : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpPower;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _rigidbody.AddForce(new Vector3(0, _jumpPower, 0), ForceMode.Impulse);
        }

        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.AddForce(new Vector3(0, 0, _speed));
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.AddForce(new Vector3(0, 0, -_speed));
        }

        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.AddForce(new Vector3(-_speed, 0, 0));
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.AddForce(new Vector3(_speed, 0, 0));
        }
    }
}
