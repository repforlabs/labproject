using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBonusController : MonoBehaviour
{
    [SerializeField] private int _jumpPower = 20;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Rigidbody rigidbody = other.GetComponent<Rigidbody>();
            rigidbody.AddForce(Vector3.up * _jumpPower, ForceMode.Impulse);
            Destroy(gameObject);
            //gameObject.SetActive(false);
        }
    }
}
