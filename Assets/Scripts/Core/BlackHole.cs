using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    [SerializeField] private int _power = 3;
    [SerializeField] private ForceMode _mode;
    [SerializeField] private AnimationCurve _curve;

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Rigidbody moveableObject = other.GetComponent<Rigidbody>();
            float distance = Vector3.Distance(moveableObject.position, transform.position);
            Vector3 pullVector = (transform.position - moveableObject.position) / (_power * _curve.Evaluate(distance) );
            moveableObject.AddForce(pullVector, _mode);
        }
    }
}
