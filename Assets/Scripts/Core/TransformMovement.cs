using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 2;
    [SerializeField] private int _rotSpeed = 2;
    [SerializeField] private Transform _ground;
    [Range(0, 1)][SerializeField] private float _time;

    [SerializeField] private Transform[] _points;
    [SerializeField] private GameObject[] _gameObjects;
    [SerializeField] private int[] _numbers;
    void Start()
    {
        //transform.position = new Vector3(1, 9, 1);
        //transform.localScale = new Vector3(2, 6, 8);
    }

    void Update()
    {
        //LagMove();
        //NormalMove();
        //RotatePlayer();
        //LerpMove();
        //transform.Rotate(new Vector3(38, 70, 50));
        //transform.LookAt(_ground);
        //if (Input.GetKey(KeyCode.Space))
        //{
        //    transform.localScale += new Vector3(1, 1, 1);
        //}
        //if(transform.position == transform.position);
        ToPointMovement();
    }

    private void ToPointMovement()
    {

        transform.position = Vector3.MoveTowards(transform.position, _points[0].position, _speed);
    }

    private void LerpMove()
    {
        transform.position = new Vector3(Mathf.Lerp(0, 10, _time), Mathf.Lerp(0, 10, _time), Mathf.Lerp(0, 10, _time));
        _time += Time.deltaTime;
        if (_time > 1)
        {
            _time = 0;
        }
    }

    private void NormalMove()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * _speed * Time.deltaTime);
        }
    }

    private void RotatePlayer()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up * Time.deltaTime * _rotSpeed);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.down * Time.deltaTime * _rotSpeed);
        }
    }

    private void LagMove()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            transform.position += Vector3.forward * _speed;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            transform.position += Vector3.back * _speed;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.position += Vector3.right * _speed;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            transform.position += Vector3.left * _speed;
        }
    }
}
