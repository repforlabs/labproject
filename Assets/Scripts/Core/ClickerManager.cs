using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickerManager : MonoBehaviour
{
    [SerializeField] private float _coins;
    private int _clickModifier = 1;
    private int _clickUpgradeCost = 10;
    void Start()
    {
        InvokeRepeating("AddCoin", 1, 1);
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _coins += _clickModifier;
            Debug.Log("Coins: " + _coins);
        }
        if (Input.GetKeyDown(KeyCode.U) && _coins >= _clickUpgradeCost)
        {
            _clickModifier++;
            _coins -= _clickUpgradeCost;
            _clickUpgradeCost += _clickUpgradeCost;
            Debug.Log("Coins: " + _coins);
            Debug.Log("Click upgrade cost: " + _clickUpgradeCost);
            Debug.Log("Current click power: " + _clickModifier);
        }
        else if(Input.GetKeyDown(KeyCode.U))
        {
            Debug.Log("Click upgrade cost: " + _clickUpgradeCost);
            Debug.Log("Current click power: " + _clickModifier);
        }
    }

    private void AddCoin()
    {
        _coins+=_clickModifier;
    }
}
