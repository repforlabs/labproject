using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationsLauncher : MonoBehaviour
{
    private const string JUMP = "Jump";
    private const string MOVE = "Move";
    [SerializeField] private Animator _animator;

    public void PlayJump()
    {
        _animator.SetTrigger(JUMP);
    }
    public void PlayWalk(bool moving)
    {
        _animator.SetBool(MOVE, moving);
    }
}
