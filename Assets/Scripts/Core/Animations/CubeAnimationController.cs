using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeAnimationController : MonoBehaviour
{
    private const string ROTATE = "Rotate";
    private const string SCALE = "Scale";

    [SerializeField] private Animator _animator;
    void Update()
    {
        PlayAnimation(KeyCode.Return, ROTATE);
        PlayAnimation(KeyCode.Space, SCALE);
    }

    private void PlayAnimation(KeyCode keyCode, string name)
    {
        if (Input.GetKeyDown(keyCode))
            _animator.SetTrigger(name);
    }
}
