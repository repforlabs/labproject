using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sdad : MonoBehaviour
{
    public float movementSpeed = 5.0f;
        Rigidbody rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    void Update() 
    {
        PlayerPrefs.SetInt("Levels", PlayerPrefs.GetInt("Levels") + 1);
        SceneManager.LoadScene(PlayerPrefs.GetInt("Levels"));
        
        
        
        
        
        
        
        
        
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.velocity += Vector3.forward * Time.deltaTime * movementSpeed;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rigidbody.velocity += Vector3.back * Time.deltaTime * movementSpeed;
        }
         if (Input.GetKey(KeyCode.A))
        {
            rigidbody.velocity += Vector3.left * Time.deltaTime * movementSpeed;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rigidbody.velocity += Vector3.right * Time.deltaTime * movementSpeed;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddForce(Vector3.up * movementSpeed);
        }
    }
}
